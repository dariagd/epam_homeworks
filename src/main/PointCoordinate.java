package main;

import java.util.Scanner;

public class PointCoordinate {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Put x = ");
        float x = scanner.nextFloat();
        System.out.print("\nPut y = ");
        float y = scanner.nextFloat();
        System.out.println(findLocation(x, y));
    }

    static String findLocation(float x, float y){
        if(x != 0 && y != 0) {
            if (x > 0) return y > 0 ? "I quadrant" : "IV quadrant";
            else return y > 0 ? "II quadrant" : "III quadrant";
        } else if (y != 0) return "X axis";
        else if (x != 0) return "Y axis";
        else return "Zero point";
    }
}
